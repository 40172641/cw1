﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    class Student
    {
        //Fields
        private string firstName;
        private string lastName;
        private string dateOfBirth;
        private string course;
        private int matriculationNumber;
        private int level;
        private int credits;

        //Default Constructor
        public Student()
            {
            firstName = "";
            lastName = "";
            dateOfBirth = "";
            course = "";
            matriculationNumber = 0;
            level = 0;
            credits = 0;
            }

        //Overload Constructor
        //Used for Set Buttons so that Inputs update student class
        public Student(string setFirstName, string setLastName, string setDoB, string setCourse, int setMatricNumber, int setLevel, int setCredits)
        {
            setFirstName = firstName;
            setLastName = lastName;
            setDoB = dateOfBirth;
            setMatricNumber = matriculationNumber;
            setCourse = course;
            setLevel = level;
            setCredits = credits;
        }

        //Declare a First Name for a property of type string
        public string FirstName
        {
            get { return firstName; } //Gets the First Name
            set { firstName = value; }
        }

        //Declare a Last Name for a property of type string
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        
        //Declare the Date of Birth for a property of type string
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }
        //Declare a Course for a property of type string
        public string Course
        {
            get { return course; }
            set { course = value; }
        }
        //Declare a Matriculation Number for a property of type Integer
        // Validation For Matriculation Number so that 40000 to 60000 are accepted and anything else is not accepted
        public int MatriculationNumber
        {
            get { return matriculationNumber; }
            set
            {
                if (matriculationNumber >= 40000 && matriculationNumber <= 60000)
                {
                    matriculationNumber = value;
                }
                else
                {
                    throw new ArgumentException("Not Within Range");
                }
                matriculationNumber = value;
            }
        }
        //Declare a Level for a property of type Integer
        //With Validation For Level so that Levels between 1 and 4 will be accepted, and anything else with throw and exception
        public int Level
        {
            get { return level; }
            set
            {
                if (level >= 1 && level <= 4)
                {
                    level = value;
                }
                else
                {
                    throw new ArgumentException("Not Within Range");
                }

                level = value;
            }
        }
        //Declare Credits for a property of type Integer
        //Validation so Credits between 0 and 480 are accepted with anything else throwing an acception 
        public int Credits
        {
            get { return credits; }
            set
            {
                if (credits >= 0 && credits <= 480)
                {
                    credits = value;
                }
                else
                {
                    throw new ArgumentException("Not Within Range");
                }
            }
        }
        //Advance method which advances a student due to the amount of credits they have achieved
     public string Advance(int level, int credits)
        {
          string level1 = "Level 1";
          string level2 = "Level 2";
          string level3 = "level 3";
          string level4 = "level 4";

            if (credits < 120 && level == 1)
            {
                return level1;
            }
            else if (credits >= 120 && credits <= 239 && level == 2)
            {
                return level2; 
            }
           else if (credits >= 240 && credits <= 359 && level == 3)
            {
                return level3;
            }
            else if (credits >= 360 && credits <= 479 && level == 4)
            {
                return level4;
            }    
        }
    }

    //Award Method which determines the award the student will recieve depending on their amount of Credits
    public string Award(int credits)
    { 
        string higherEducation = "Higher Education";
        string degree = "Degree";
        string honourDegree = "Honour's Degree";
    
        if (credits >= 0 && credits <= 359)
        {
            return higherEducation;
        } else if (credits >= 360 && credits <= 479)
        {
            return degree;
        } else if (credits >= 480)
        {
            return honourDegree;
        } else if (credits > 800)
        {
            throw new ArgumentException("Not Within Range");
        }
    }
}
    