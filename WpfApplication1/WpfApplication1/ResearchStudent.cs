﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    class ResearchStudent : Student
    {

        //Fields
        private string topic;
        private string supervisor;


        //Default Constructor
        public ResearchStudent()
        {
            topic = "";
            supervisor = "";
        }
        //Overload Constructor
        public ResearchStudent(string setTopic, string setSupervisor)
        {
            setTopic = topic;
            setSupervisor = supervisor;
        }
        //Declare a topic for a property of type string
        public string Topic
        {
            get { return topic; }
            set { topic = value; }
        }
        //Declare a Supervisor for a property of type string
        public string Supervisor
        {
            get { return supervisor; }
            set { supervisor = value; } 
        }

    }
}
