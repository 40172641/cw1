﻿namespace cw1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMatriNo = new System.Windows.Forms.Label();
            this.lblAward = new System.Windows.Forms.Label();
            this.lblCourseAward = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMatriNo
            // 
            this.lblMatriNo.AutoSize = true;
            this.lblMatriNo.Location = new System.Drawing.Point(25, 30);
            this.lblMatriNo.Name = "lblMatriNo";
            this.lblMatriNo.Size = new System.Drawing.Size(107, 13);
            this.lblMatriNo.TabIndex = 0;
            this.lblMatriNo.Text = "Matriculation Number";
            // 
            // lblAward
            // 
            this.lblAward.AutoSize = true;
            this.lblAward.Location = new System.Drawing.Point(25, 96);
            this.lblAward.Name = "lblAward";
            this.lblAward.Size = new System.Drawing.Size(37, 13);
            this.lblAward.TabIndex = 1;
            this.lblAward.Text = "Award";
            // 
            // lblCourseAward
            // 
            this.lblCourseAward.AutoSize = true;
            this.lblCourseAward.Location = new System.Drawing.Point(25, 65);
            this.lblCourseAward.Name = "lblCourseAward";
            this.lblCourseAward.Size = new System.Drawing.Size(40, 13);
            this.lblCourseAward.TabIndex = 2;
            this.lblCourseAward.Text = "Course";
            this.lblCourseAward.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(358, 172);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 207);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblCourseAward);
            this.Controls.Add(this.lblAward);
            this.Controls.Add(this.lblMatriNo);
            this.Name = "Form1";
            this.Text = "Award";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMatriNo;
        private System.Windows.Forms.Label lblAward;
        private System.Windows.Forms.Label lblCourseAward;
        private System.Windows.Forms.Button btnClear;
    }
}