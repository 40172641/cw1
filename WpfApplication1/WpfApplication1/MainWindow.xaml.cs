﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        public MainWindow()
        {
            InitializeComponent();
        }
//Set Button
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            //Create Student Object
            string setFirstName = textBoxFirstName.Text;
            string setLastName = textBoxLastName.Text;
            string setDoB = textBoxDoB.Text;
            string setCourse = textBoxCourse.Text;
            int setCredits = Convert.ToInt32(textBoxCredits.Text);
            int setLevel = Convert.ToInt32(textBoxLevel.Text);
            int setMatricNumber = Convert.ToInt32(textBoxMatricNumber.Text);
            string setTopic = textBoxTopic.Text;
            string setSupervisor = textBoxSupervisor.Text;

            //Student Object
            Student student_1 = new Student(setFirstName, setLastName, setDoB, setCourse, setMatricNumber, setLevel, setCredits);
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            textBoxFirstName.Clear();//Clears First Name Textbox 
            textBoxLastName.Clear(); //Clears Last Name Textbox 
            textBoxDoB.Clear(); //Clears DoB Textbox
            textBoxCourse.Clear(); //Clears Course Textbox
            textBoxCredits.Clear(); //Clears Credits textbox
            textBoxLevel.Clear();//Clears Level Textbox
            textBoxMatricNumber.Clear(); //Clears Matriculation Number
            textBoxTopic.Clear(); //Clears Topic Textbox
            textBoxSupervisor.Clear(); //Clears Supiervisor Textbox
        }

        private void btnAdvance_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void btnAward_Click(object sender, RoutedEventArgs e)
        {
            //Opens the Form For the Award Window
            Hide();
            AwardForm openForm = new AwardForm();
            openForm.Show();
        }
    }
}
