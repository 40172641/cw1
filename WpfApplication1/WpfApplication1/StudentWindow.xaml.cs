﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1
{
    /// <summary>
    /// Interaction logic for StudentWindow.xaml
    /// </summary>
    public partial class StudentWindow : Window
    {
        public StudentWindow()
        {
            InitializeComponent();
        }

        private void buttonSet_Click(object sender, RoutedEventArgs e)
        {

            //Create Student Object
            string setFirstName = textBoxFirstName.Text;
            string setLastName = textBoxCourse.Text;
            string setDoB = textBoxDoB.Text;
            string setCourse = textBoxCourse.Text;
            int setMatricNumber = Convert.ToInt32(textBoxMatricNumber.Text);
            int setLevel = Convert.ToInt32(textBoxLevel.Text);
            int setCredits = Convert.ToInt32(textBoxCredits.Text);

            //New Student Object 
            Student newStudent = new Student(setFirstName, setLastName, setDoB, setCourse, setMatricNumber, setLevel, setCredits);
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            textBoxFirstName.Clear();//Clears First Name Textbox 
            textBoxLastName.Clear(); //Clears Last Name Textbox 
            textBoxDoB.Clear(); //Clears DoB Textbox
            textBoxCourse.Clear(); //Clears Course Textbox
            textBoxMatricNumber.Clear(); //Clears Matriculation Number
            textBoxCredits.Clear(); //Clears Credits textbox
            textBoxLevel.Clear();//Clears Level Textbox
            textBoxTopic.Clear(); //Clears Topic Textbox
            textBoxSupervisor.Clear(); //Clears Supiervisor Textbox
        }
    }
}