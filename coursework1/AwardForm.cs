﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Kevin Falconer 40172641
//Award Form - Displays the Award form 
//Last Date Modified : 23 October 2015
namespace coursework1
{
    public partial class AwardForm : Form
    {
        //Outputs the Input from the Student Class and Award
        public AwardForm(int MatriculationNumber, string FirstName, string LastName, int Level, string Course, int Credits)
        {
            InitializeComponent();
            textBoxMatricAward.Text = MatriculationNumber.ToString(); // Outputs the Matriculation Number
            textBoxLastNameAward.Text = LastName; //Outputs the Last Name
            textBoxFirstNameAward.Text = FirstName; //Outputs the First Name
            textBoxCourseAward.Text = Course; //Outputs the Course of the Student
            textBoxLevelAward.Text = Level.ToString(); //Outputs the Level

            //Award Method
            if (Credits >= 0 && Credits <= 359)
            {
                textBoxAward.Text = "Certificate of Higher Education"; // Updates textbox with Certificate of Higher Education
            }
            else if (Credits >= 360 && Credits <= 479)
            {
                textBoxAward.Text = "Degree"; //Updates textbox with Degree
            }
            else if (Credits >= 480)
            {
                textBoxAward.Text = "Honour's Degree"; // Updates textbox with Honours Degree
            }


        }

        public MainWindow MainWindow
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        //Closes the Award Form
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close(); //Closes the Award Form
        }
    }
}

