﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//Kevin Falconer 40172641
//Student Class - Stores Student Details
//Last Date Modified : 23 October 2015
namespace coursework1
{
    public class Student
    {
        //Fields
        private string firstName;
        private string lastName;
        private string dateOfBirth;
        private string course;
        private int matriculationNumber;
        private int level;
        private int credits;


        //Declare a First Name for a property of Type String
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        //Declare a Last Name for a property of Type String
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        //Declare a Date of Birth for a property of type String 
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }

        //Declare a Course for a property of type String
        public string Course
        {
            get { return course; }
            set { course = value; }
        }
        //Declare a matriculation Number of type Integer
        //Validation for Matric Number so only values between 40000 and 60000 are accepted
        public int MatriculationNumber
        {
            get
            {
                return matriculationNumber;
            }
            set
            {
                if (MatriculationNumber < 40000 && MatriculationNumber > 60000)
                {
                    throw new ArgumentException("Not within Matricualtion Number Range"); //Throws exception if Input isn't between 40000 and 60000
                }
                matriculationNumber = value;
            }
        }
        // Declare a Level of Type Integer
        // Validation so that inputs between 1 and 4 are only accepted
        public int Level
        {
            get
            {
                return level;
            }
            set
            {
                if (Level < 1 && Level > 4)
                {
                    throw new ArgumentException("Not Within Numeric Range"); //Throws exception if Input isn't between 1 and 4
                }
                else
                {
                    level = value;

                } //End of Loop
            }
        }
        //Delare Credits of Type Integer
        //Validation so that Inputs between 0 and 480 are only Accepted
        public int Credits
        {
            get
            {
                return credits; //returns credits
            }
            set
            {
                if (Credits < 0 && Credits > 480) //Validation so only values between 0 and 480 are accepted
                {
                    throw new ArgumentException("Not within Creidts range!!!"); //Throws exception if Input isn't between 0 and 480
                }
                credits = value;
                } // End of Loop
                
            }

        public MainWindow MainWindow
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }
    }
    
} // End of Class
        