﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//Kevin Falconer 40172641
//Main Window Class - Outputs the GUI with all textboxes, labels, and buttons
//Last Date Modified : 23 October 2015
namespace coursework1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
        }

        Student studentObject = new Student(); //Creats new object called studentObject

        //Set Buttons takes in the User input and updates the variables in the student class using the student object
        private void buttonSet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                studentObject.FirstName = textBoxFirstName.Text; //Inputs First Name and updates student class
                studentObject.LastName = textBoxLastName.Text; // Inputs Last Name and updates student class
                studentObject.DateOfBirth = textBoxDoB.Text; //Inputs Date of Birth and updates student class
                studentObject.Course = textBoxCourse.Text; //Inputs course name and updates student class
                studentObject.MatriculationNumber = Convert.ToInt32(textBoxMatricNumber.Text); //inputs matriculation number and updates student class
                studentObject.Level = Convert.ToInt32(textBoxLevel.Text); // Inputs student level and updates student class
                studentObject.Credits = Convert.ToInt32(textBoxCredits.Text); //Inputs students credits and updates student class
            }
            catch (Exception except)
            {
                MessageBox.Show(except.Message); //Outputs exception Erros
            }
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            textBoxFirstName.Clear();//Clears First Name TextBox
            textBoxLastName.Clear(); //Clears Last Name TextBox
            textBoxDoB.Clear();//Clears DoB textbox 
            textBoxCourse.Clear();//Clears the Course Textbox
            textBoxMatricNumber.Clear();//Clears MatricNumber textbox
            textBoxLevel.Clear();//Clears Level TextBox
            textBoxCredits.Clear(); //Clears Credits Textbox

        }

        private void buttonGet_Click(object sender, RoutedEventArgs e)
        {
            //Updates TextBoxes with Methods


            textBoxFirstName.Text = studentObject.FirstName; //Updates First Name textbox
            textBoxLastName.Text = studentObject.LastName; //Updates Last Name textbox
            textBoxDoB.Text = studentObject.DateOfBirth; //Updates Date of Birth Textbox
            textBoxCourse.Text = studentObject.Course; // Updates Course textbox
            textBoxMatricNumber.Text = studentObject.MatriculationNumber.ToString(); //Updates Matriculation Number Textbox
            textBoxLevel.Text = studentObject.Level.ToString(); // Updates Level Textbox
            textBoxCredits.Text = studentObject.Credits.ToString(); // Updates Credits Textbox


        }

        private void buttonAdvance_Click(object sender, RoutedEventArgs e)
        {
            //Advance button
            if (studentObject.Level == 1 && studentObject.Credits >= 120)
            {
                textBoxLevel.Text = "2"; // Updates Level textbox to 2
                studentObject.Level = 2; // Updates StudentObject.Level to 2
            }
            else if (studentObject.Level == 2 && studentObject.Credits >= 240)
            {
                textBoxLevel.Text = "3"; //Updates the Level textbox to 3
                studentObject.Level = 3; // Updates StudentObject.Level to 3
            }
            else if (studentObject.Level == 3 && studentObject.Credits >= 360)
            {
                textBoxLevel.Text = "4"; //Updates the Level textbox to 4
                studentObject.Level = 4; // Updates the StudentObject.Level to 4

            }
            else if (studentObject.Level == 4)
            {
                MessageBox.Show("Student Already in Year 4");
                studentObject.Level = 4; //StudentObject.Level stays the Same
            }
            else if (studentObject.Level == 1 && studentObject.Credits < 120)
            {
                MessageBox.Show("Credits is below 120 so cannot advance to Level 2");
                studentObject.Level = 1; //StudentObject.Level stays the Same
            }
            else if (studentObject.Level == 2 && studentObject.Credits < 240)
            {
                MessageBox.Show("Credits is below 240 so cannot advance to Level 3");
                studentObject.Level = 2; //StudentObject.Level stays the Same
            }
            else if (studentObject.Level == 3 && studentObject.Credits < 360)
            {
                MessageBox.Show("Credits is below 360, so Cannot advance to Level 4");
                studentObject.Level = 3; //StudentObject.Level stays the Same
            }

        }

        private void buttonAward_Click(object sender, RoutedEventArgs e)
        {

            
            AwardForm award = new AwardForm(Convert.ToInt32(textBoxMatricNumber.Text), textBoxLastName.Text, textBoxFirstName.Text, Convert.ToInt32(textBoxLevel.Text), textBoxCourse.Text, Convert.ToInt32(textBoxCredits.Text));
            award.Hide();
            award.Show();

            if (studentObject.Credits >= 0 && studentObject.Credits <= 359)
            {
                MessageBox.Show("You've recieved a Cetificate ofHigher Education"); //Message shows when Award Button is clicked
            }
            else if (studentObject.Credits >= 0 && studentObject.Credits <= 479)
            {
                MessageBox.Show("You've recieved a Degree"); //Message shows when Award Button is clicked
            }
            else if (studentObject.Credits >= 480)
            {
                MessageBox.Show("You've Recieved an Honour's Degree"); //Message shows when Award Button is clicked
            }//End of Loop

        }//End of Class


    }
}