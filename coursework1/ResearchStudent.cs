﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Kevin Falconer 40172641
//Research Student Class - Stores the detials of the Research Student CLass
//Last Date Modified : 23 October 2015
namespace coursework1
{
    class ResearchStudent : Student //Inherits the Student Class
    {

        //Fields
        private string topic;
        private string supervisor;

 

        //Declare a topic for a property of type string
        public string Topic
        {
            get { return topic; }
            set { topic = value; }
        }

        //Declare a Supervisor for a property of type string 
        public string Supervisor
        {
            get { return supervisor; }
            set { supervisor = value; }
        }


        }
    }
//End of Class
